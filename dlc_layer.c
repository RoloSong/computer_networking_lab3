/* ----------- PROGRAMMING ASSIGNMENT 3  ----------------- 
 * Name:Qi Song
 * e-mail:song.1602@buckeyemail.osu.edu
 * CSE account:song.1602
 * ---------------------------------------------------- */
/* --- DO NOT REMOVE OR MODIFY  #include STATEMENTS BELOW --- */

#include "cisePort.h"
#include "sim.h"
#include "component.h"
#include "comptypes.h"
#include "list.h"
#include "eventdefs.h"
#include "main.h"
#include "route_activity.h"
#include "sim_tk.h"
#include "dlc_layer.h"

/****************************************************/
/* --- YOU DO NOT HAVE TO HAVE THIS FUNCTION --- */
static int
window_open(DLC_Conn_Info_TYPE *dci)
{
 int result;
	printf("sndnxt = %d, una = %d\n",dci->snd_nxt,dci->snd_una); 
  /* Based on a number of a_pdu's in the transmission buffer 
      and values for snd_nxt, snd_una and window_size    
      determine if there is a new a_pdu ready to be sent */
 if (DataInPDUBuffer(dci) == 0)
	//if there is no a_pdu in the transmission buffer, 
	//then dont send 
	return 0;
 if(dci->snd_nxt - dci->snd_una >= dci->window_size)
	//if we already reach the max window size
	//dont send
	return 0;
 if(dci->snd_nxt < dci->snd_una && dci->snd_nxt + dci->window_size - dci->snd_una > dci->window_size)
	//if the next sequence number is in the next roll
	//and the max winsow size is reached
	//then dont send
	return 0;
 if(dci->snd_nxt >= dci->snd_una && dci->snd_nxt - dci->snd_una >= DataInPDUBuffer(dci))
	//if all of the buffered data units are all sent but unacked
	//dont send
	return 0;
 if(dci->snd_nxt < dci->snd_una && dci->window_size + dci->snd_nxt - dci->snd_una >= DataInPDUBuffer(dci))
	//if all of the buffered data units are all sent but not acked
	//and the next sequence number is in next roll
	//dont send
	return 0;
 if(dci->snd_una -1 == dci->snd_nxt || (dci->snd_una == 0 && dci->snd_una == 7))
	return 0;
 return 1; // result = 1, there is an a_pdu ready
                // result = 0, there is not
}

/**************************************************************/
/* --- DO NOT REMOVE OR MODIFY THIS FUNCTION --- */
static 
dlc_layer_receive(DLC_LAYER_ENTITY_TYPE *dlc_layer_entity,
		  GENERIC_LAYER_ENTITY_TYPE *generic_layer_entity,
		  PDU_TYPE *pdu)
{
	/* Gets the appropriate  DLC_Conn_Info_TYPE structure */
	DLC_Conn_Info_TYPE *dci;
	dci = Datalink_Get_Conn_Info(dlc_layer_entity,pdu);
      
	if (DatalinkFromApplication(generic_layer_entity)) {
	    FromApplicationToDatalink(dlc_layer_entity, pdu, dci);
	} else if (DatalinkFromPhysical(generic_layer_entity)) {
	    FromPhysicalToDatalink(dlc_layer_entity, pdu, dci);
	}
	return 0;
}




/**************************************************************/
/* --- YOU MUST HAVE THIS FUNCTION --- */
static
FromApplicationToDatalink(DLC_LAYER_ENTITY_TYPE *dlc_layer_entity, PDU_TYPE *pdu_from_application,
				   DLC_Conn_Info_TYPE *dci)
{
  /* Insert the pdu from the applicaiton layer to the 
     transmission buffer. */
	InsertPDUIntoBuffer(dlc_layer_entity, pdu_from_application, dci);
  /* If possible send info frame. */	
	while(window_open(dci)){
		SendInfo(dlc_layer_entity, dci);
	}
}

/**************************************************************/
/* --- YOU MUST HAVE THIS FUNCTION --- */
static
FromPhysicalToDatalink(DLC_LAYER_ENTITY_TYPE *dlc_layer_entity,
					  PDU_TYPE *pdu_from_physical,
					  DLC_Conn_Info_TYPE *dci)
{
	printf("%d:receive some thing\n",GetNodeID(dlc_layer_entity));
	/* Check and discard the pdu when error */
	//if the pdu is not error free or is not pdu type, discard
	if(pdu_from_physical->type != TYPE_IS_D_PDU){
		pdu_free(pdu_from_physical);
		printf("discarded for type, %d, %d\n",pdu_from_physical->type, TYPE_IS_D_PDU);
		return;
	}
	if(pdu_from_physical->u.d_pdu.error != NO){
		printf("discarded for error\n");
		pdu_free(pdu_from_physical);
		return;
	}
	/* If not discarded, check d_pdu.type and call an 
        appropriate function: 
	   if RR you may use and call DataLinkProcessRR()
	   if REJ you may use and call DataLinkProcessREJ()
	   if INFO you may use and call DataLinkProcessINFO() */
	if(pdu_from_physical->u.d_pdu.type == D_RR){
		//if type is RR, then call process rr
		DatalinkProcessRR(dlc_layer_entity, pdu_from_physical,dci);
	}	
	else if (pdu_from_physical->u.d_pdu.type == D_REJ){
		//if type is REJ, then call process REJ
		DatalinkProcessREJ(dlc_layer_entity, pdu_from_physical, dci);
	}
	else if (pdu_from_physical->u.d_pdu.type == D_INFO){
		//if type is INFO, then call process INFO
		DatalinkProcessInfo(dlc_layer_entity, pdu_from_physical, dci);
	}
}

/**************************************************************/
/* --- YOU DO NOT HAVE TO HAVE THIS FUNCTION --- */
static
DatalinkProcessRR(DLC_LAYER_ENTITY_TYPE *dlc_layer_entity,
				  PDU_TYPE *pdu,
				  DLC_Conn_Info_TYPE *dci)
{
	printf("%d: received RR for %d, not processed\n",GetNodeID(dlc_layer_entity),pdu->u.d_pdu.number);
	if(pdu->u.d_pdu.address != GetNodeID(dlc_layer_entity)){
	//if the address does not match, then discard
		pdu_free(pdu);
		return 0;
	}
	
     /* If this is a response RR with P/F_bit=0:
        Free up space in the retransmission buffer. Use:
	   UpdatePDUBuffer(dlc_layer_entity,pdu,dci); 
        Update snd_una
	   Send as many info pdu's as allowed by window. You may 
        use: window_open(dci)and SendInfo(dlc_layer_entity, dci); 
     */
	if(pdu->u.d_pdu.p_bit == NO){
	//free up space
		UpdatePDUBuffer(dlc_layer_entity,pdu,dci);
	//update snd_una
		dci->snd_una = pdu->u.d_pdu.number;
		printf("processing RR, una is %d, next is %d\n",dci->snd_una,dci->snd_nxt);
	//send available frames
		while(window_open(dci))
		{	
			SendInfo(dlc_layer_entity, dci);
		}
		printf("sender receiving RR for %d\n",pdu->u.d_pdu.number);
		pdu_free(pdu);
		return 0;
	}

     /* If this is a command RR with P/F_bit=1:
        Free up space in the retransmission buffer.
        Clear rej_already_sent 
        Update snd_una
	   Send as many info pdu's as allowed by window. 
        Create and send to physical layer a response RR with P/F_=1
     */
	if(pdu->u.d_pdu.p_bit == YES && dci->rr_pbit_sent == 0){
	//if the current node didn't send a RR with p = 1
	//then we can assume that current RR frame is a command RR
		printf("this is a command RR\n");	
	//free up space
//		UpdatePDUBuffer(dlc_layer_entity, pdu, dci);
	//update snd_una and set rej_pbit_sent to 0
//		dci->snd_una = pdu->u.d_pdu.number;
	//unsend any unsent a_pdu
//		while(window_open(dci))
//			SendInfo(dlc_layer_entity,dci);
	//send RR response
		PDU_TYPE * rr_pdu;
		rr_pdu = pdu_alloc();
		rr_pdu->type = TYPE_IS_D_PDU;
		rr_pdu->u.d_pdu.type = D_RR;
		rr_pdu->u.d_pdu.address = GetReceiverID(dlc_layer_entity);
		rr_pdu->u.d_pdu.number = dci->rcv_nxt;
		rr_pdu->u.d_pdu.p_bit = YES;
		rr_pdu->u.d_pdu.error = NO;
		send_pdu_to_physical_layer(dlc_layer_entity, rr_pdu);
		pdu_free(pdu);
		return 0;
	}     
     /* If this is response RR with P/F_bit=1 and rr_pbit=1:
        Free up space in the retransmission buffer.
        Clear rr_pbit_sent 
        Update snd_una and snd_nxt
        Send as many info pdu's as allowed by window.
     */

	if(pdu->u.d_pdu.p_bit == YES && dci->rr_pbit_sent == 1){
	//if the current node did send a RR frame with p_bit = 1
	//we assume that the incomming frame is a response to the pdu we've sent
		UpdatePDUBuffer(dlc_layer_entity, pdu, dci);
		dci->snd_una = pdu->u.d_pdu.number;
		dci->rr_pbit_sent = 0;
		dci->snd_nxt = dci->snd_una;
		printf("this is reponse rr, snd_nxt = %d, una = %d\n",dci->snd_nxt, dci->snd_una);
	//send lost frames
		while(window_open(dci))
			SendInfo(dlc_layer_entity, dci);
		pdu_free(pdu);
		return 0;
	}

     /* Ignore and discard all other RR�s */
	/* Free pdu */
	pdu_free(pdu);
	return 0;
}

/*************************************************************/
/* --- YOU DO NOT HAVE TO HAVE THIS FUNCTION --- */
static
DatalinkProcessREJ(DLC_LAYER_ENTITY_TYPE *dlc_layer_entity,
				   PDU_TYPE *pdu, DLC_Conn_Info_TYPE *dci)
{
	// check address and p-bit
	if(GetNodeID(dlc_layer_entity) != pdu->u.d_pdu.address){
		pdu_free(pdu);
		return 0;
	}
	if(pdu->u.d_pdu.p_bit != NO){
		pdu_free(pdu);
		return 0;
	}
  /* Check the address and P/F_bit and if not correct discard frame 
     Otherwise, free up space in the retransmission buffer 
     update snd_una and snd_nxt 
   Send as many pdu's as allowed by window 
  */
	UpdatePDUBuffer(dlc_layer_entity, pdu, dci);
	dci->snd_una = pdu->u.d_pdu.number;
	dci->snd_nxt = dci->snd_una;
	while(window_open(dci))
		SendInfo(dlc_layer_entity, dci);
	pdu_free(pdu);
  /* Free pdu */		
    return 0;
}

/**************************************************************/
/* --- YOU DO NOT HAVE TO HAVE THIS FUNCTION --- */
static
DatalinkProcessInfo(DLC_LAYER_ENTITY_TYPE *dlc_layer_entity,
					PDU_TYPE *pdu,
					DLC_Conn_Info_TYPE *dci)
{
	printf("receiver receive %d \n",pdu->u.d_pdu.number);
	printf("receiver current looking for %d\n",dci->rcv_nxt);
	PDU_TYPE *pdu_to_application;
     
 /* Check the address and P/F_bit and if not correct discard frame */
     	if(pdu->u.d_pdu.address != GetNodeID(dlc_layer_entity)){
		printf("discard 00\n");
		pdu_free(pdu);
		return 0;
	}
	if(pdu->u.d_pdu.p_bit != NO){
		printf("discard 0\n");
		pdu_free(pdu);
		return 0;
	}
	/* Check if the pdu has the expected sequence number
        When out of sequence, then send REJ, discard pdu and 
        return 0 */
	if(pdu->u.d_pdu.number < dci->rcv_nxt ){
		//discard pdu 
		printf("discarded1\n");
		pdu_free(pdu);
		return 0;
	}
	if(pdu->u.d_pdu.number > dci->rcv_nxt){
		printf("discarded2\n");
		SendREJ(dlc_layer_entity,pdu,dci);
		pdu_free(pdu);
		return 0;
	}
	/* If expected PDU, then increment rcv_nxt,
        clear rej_already_sent and RR is sent 
	   You may use SendRR() to send RR to physical*/
	//increment the rcv_nxt, if it exceeds the window_size,
	//reduce it to 0
	int next_seq = dci->rcv_nxt +1;
	if(next_seq > dci->window_size)
		next_seq = 0;
	dci->rcv_nxt = next_seq;
	dci->rej_already_sent = NO;
	printf("receiver prepare to send RR, looking for %d\n",dci->rcv_nxt);
	SendRR(dlc_layer_entity, pdu, dci);
     /* --- Send pdu to application */
	pdu_to_application = pdu_alloc();
	pdu_to_application->type = TYPE_IS_A_PDU;
	memcpy(pdu_to_application->u.a_pdu.data, pdu->u.d_pdu.a_pdu.data, DATASIZE);
	send_pdu_to_application_layer(dlc_layer_entity, pdu_to_application);
	/* Free pdu */
	pdu_free(pdu);
	printf("ends of receiving\n");
	return 0;
}

/**************************************************************/
/* --- DO NOT CHNAGE NAME OF THIS FUNCTION --- */
/* The function is automatically called when the timer expires*/
static
DatalinkTimerExpired(DLC_LAYER_ENTITY_TYPE *dlc_layer_entity,
					 DLC_Conn_Info_TYPE *dci)
{
	/* Only if there are a_pdu in the buffer, allocate a new d_pdu 
        and fill in the needed fields for a command RR with P_Fbit=1 
        Set rr_pbit_sent=1
	  Send to d_pdu to physical layer 
      */
	printf("time out function called\n");
     	if(DataInPDUBuffer(dci) != 0)
	{
		dci->rr_pbit_sent =1;
		PDU_TYPE * rr_pdu;
		rr_pdu = pdu_alloc();
		rr_pdu->type = TYPE_IS_D_PDU;
		rr_pdu->u.d_pdu.type = D_RR;
		rr_pdu->u.d_pdu.address = GetReceiverID(dlc_layer_entity);
		rr_pdu->u.d_pdu.number = dci->rcv_nxt;
		rr_pdu->u.d_pdu.p_bit = YES;
		rr_pdu->u.d_pdu.error = NO;
		send_pdu_to_physical_layer(dlc_layer_entity, rr_pdu);
		SetTimer(dlc_layer_entity,dci);
	}
	return 0;
}

/**************************************************************/
/* --- YOU DO NOT HAVE TO HAVE THIS FUNCTION --- */
static
SendInfo(DLC_LAYER_ENTITY_TYPE *dlc_layer_entity,
			  DLC_Conn_Info_TYPE *dci)
{	printf("send info %d\n",dci->snd_nxt);
     /* get a_pdu to send */
	/* Copy it to a new d_pdu and fill the remaining fields */
	PDU_TYPE * pdu_to_send;
	pdu_to_send = GetPDUFromBuffer(dci);
	PDU_TYPE * pdu_to_physical;
	pdu_to_physical = pdu_alloc();
	pdu_to_physical->type = TYPE_IS_D_PDU;
	pdu_to_physical->u.d_pdu.type = D_INFO;
	pdu_to_physical->u.d_pdu.address = GetReceiverID(dlc_layer_entity);
	pdu_to_physical->u.d_pdu.error = NO;
	pdu_to_physical->u.d_pdu.p_bit = NO;
	pdu_to_physical->u.d_pdu.number = dci->snd_nxt;
	//printf("sender snd_nxt = %d\n",dci->snd_nxt);
	memcpy(pdu_to_physical->u.d_pdu.a_pdu.data, pdu_to_send->u.a_pdu.data, DATASIZE);
	/* increment snd_nxt */
	int nextseq = dci->snd_nxt + 1;
	if(nextseq > dci->window_size)
		nextseq = 0;
	dci->snd_nxt = nextseq;
     /* Set timer */
	/* --- Send d_pdu to physical layer */
	SetTimer(dlc_layer_entity, dci);
	send_pdu_to_physical_layer(dlc_layer_entity, pdu_to_physical);
    return 0;
}

/************************************************************/
/* --- YOU DO NOT HAVE TO HAVE THIS FUNCTION --- */
static
SendRR(DLC_LAYER_ENTITY_TYPE *dlc_layer_entity, PDU_TYPE *pdu,
	   DLC_Conn_Info_TYPE *dci)
{
	/* Allocate a new d_pdu and fill in the needed fields */
	/* Send to d_pdu to physical layer */
	//This is just a function to send normal RR,
	//RR with p_bit = 1 is send directly inline
	PDU_TYPE * rr_pdu;
	rr_pdu = pdu_alloc();
	rr_pdu->type = TYPE_IS_D_PDU;
	rr_pdu->u.d_pdu.type = D_RR;
	rr_pdu->u.d_pdu.address = GetReceiverID(dlc_layer_entity);
	rr_pdu->u.d_pdu.number = dci->rcv_nxt;
	rr_pdu->u.d_pdu.p_bit = NO;
	rr_pdu->u.d_pdu.error = NO;
	send_pdu_to_physical_layer(dlc_layer_entity, rr_pdu);
	pdu_free(pdu);
	return 0;
}

/**************************************************************/
/* --- YOU DO NOT HAVE TO HAVE THIS FUNCTION --- */
static
SendREJ(DLC_LAYER_ENTITY_TYPE *dlc_layer_entity, PDU_TYPE *pdu,
		DLC_Conn_Info_TYPE *dci)
{
	/* Don't send REJ nor RR if rej_already_sent = 1 */
	if(dci->rej_already_sent == YES)
		return 0;
	/* If REJ is to be send, allocate a new d_pdu and fill in the 
         needed fields */
	dci->rej_already_sent = YES;
	PDU_TYPE * rej_pdu;
	rej_pdu = pdu_alloc();
	rej_pdu->type = TYPE_IS_D_PDU;
	rej_pdu->u.d_pdu.address = GetReceiverID(dlc_layer_entity);
	rej_pdu->u.d_pdu.type = D_REJ;
	rej_pdu->u.d_pdu.number = dci->rcv_nxt;
	rej_pdu->u.d_pdu.p_bit = NO;
	rej_pdu->u.d_pdu.error = NO;
	send_pdu_to_physical_layer(dlc_layer_entity, rej_pdu);
	/* Send to d_pdu to physical layer */
	/* rej_already_sent set to 1 */
	pdu_free(pdu);
	return 0;
}

